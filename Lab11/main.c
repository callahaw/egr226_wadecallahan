/*Wade Callahan
 * Prof Ekin
 * EGR 226 Lab 11
 * This program will turn on an onboard LED if an IR detecting
 * Diode detect a signal of 10Hz from an IR emitting Diode.
 */

#include "msp.h"
#include "stdio.h"

int temp = 0, current = 0, frequency;


void timerA2_init(void);
void timerA0_init(void);
void  SysTick_Init();
void delayMS(uint8_t delay);
void delayUS(uint16_t delay);

void main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;     // stop watchdog timer

    SysTick_Init();
    P1->DIR |= BIT0; // set red onboard LED to output

    __disable_irq();
    timerA2_init();
    timerA0_init();

    NVIC_EnableIRQ(TA2_N_IRQn);
    __enable_irq();

    while(1)
    {
    }

}

//This function initializes a SysTick timer and sets it to run for 1 millisecond
void  SysTick_Init(){
    SysTick->CTRL   =   0;
    SysTick->LOAD   =   50 - 1;                   //1 Millisecond
    SysTick->VAL    =   21;                         //Clears with any value
    SysTick->CTRL   =   0x00000007;                       //Enables Clock
    //while((SysTick->CTRL & BIT(16))==0);            //wait for delay time to elapse
}
//This functions accepts an amount of milliseconds and calls a function to delay
//the given amount of milliseconds desired
void delayMS(uint8_t delay){
    SysTick->LOAD = (delay * 3000 - 1);            //3000 clocks = 1 milii
    SysTick->VAL = 0;                           //puts time to zero
    while((SysTick->CTRL & BIT(16))==0);       //wait for delay time to elapse
}
//This functions accepts an amount of microseconds and calls a function to delay
//the given amount of microseconds desired
void delayUS(uint16_t delay){
    SysTick->LOAD = (delay * 3 - 1);            //3 = clocks = 1 micro
    SysTick->VAL = 0;                           //puts time to zero
    while((SysTick->CTRL & BIT(16))==0);       //wait for delay time to elapse
}

void timerA2_init(void)
{

    P5->SEL0 |= BIT6;
    P5->SEL1 &= ~BIT6;
    P5->DIR &= ~BIT6;
    P5->REN |= BIT6;                                                                    // Enable Internal resistor
    P5->OUT |= BIT6;

    TIMER_A2->CTL |=        TIMER_A_CTL_SSEL__SMCLK |                           // Use SMCLK as clock source,
                            TIMER_A_CTL_MC__CONTINUOUS |                        // Start timer in UP mode
                            TIMER_A_CTL_ID_3 |                                      // divide by 8 375KHz to capture 10 & 14 Hz
                            TIMER_A_CTL_CLR;                                    // clear TA0R

    TIMER_A2->CCTL[1] =     TIMER_A_CCTLN_CM_1 |                                // Rising edge
                            TIMER_A_CCTLN_CCIS_0 |                              //
                            TIMER_A_CCTLN_CCIE |                                //capture interrupt
                            TIMER_A_CCTLN_CAP |                                 //capture mode
                            TIMER_A_CCTLN_SCS;                                  // Sync. capture

}

void timerA0_init(void)
{
            P2->SEL0 |= BIT4;                                                                               // set pin 2.4 to TA0.1, output PWM
            P2->SEL1 &= ~BIT4;
            P2->DIR |= BIT4;
            P2->OUT &= ~BIT4;

            TIMER_A0->CTL =         TIMER_A_CTL_SSEL__SMCLK |                                                   //SMCLK
                                    TIMER_A_CTL_MC__UP      |                                                   //Up
                                    TIMER_A_CTL_ID__8       |                                                   //Divide by 8
                                    TIMER_A_CTL_IE;                                                             //enables the interupt
            TIMER_A0->CCR[0] = 37500 - 1;                                                                       // generate 10Hz signal
            TIMER_A0->CCR[1] = (37500-1) * .5;                                                                  // 50% duty cycle
            TIMER_A0->CCTL[1] = TIMER_A_CCTLN_OUTMOD_7;
}

void TA2_N_IRQHandler(void) {

    current = TIMER_A2->CCR[1];                                     //sets current edge
    frequency = 1/(8*(current - temp)/3000000);                     // calculate frequency
    temp = current;                                                 //increment the previous edge
    delayUS(100);

    if((9 < frequency) && (frequency < 11)) {                       // frquency between 9 and 11
        P1->OUT |= BIT0;                                            //turn on LED
    } else {
        P1->OUT &= ~BIT0;
    }

    TIMER_A2->CCTL[1] &= ~1;                                        // reset interrupt
}
