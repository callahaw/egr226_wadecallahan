/**
 *Wade Callahan
 *Prof Ekin
 *EGR 226 Lab 8
 *This function will set a pwm based on input from debugging console
 *The function uses a systick timer for delays
 */

#include "msp.h"

//functionn prototype

void  SysTick_Init();
void delayMS(uint8_t delay);
void delayUS(uint16_t delay);
void SysTickPWM();



void main(void)
{
	WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;		// stop watchdog timer

	    SysTick_Init();                 //setsup systick
        SysTickPWM();                   //stays here infinetly
        while(1);
}


//This function initializes a SysTick timer and sets it to run for 1 millisecond
void  SysTick_Init(){
    SysTick->LOAD   =   3000 - 1;                   //1 Millisecond
    SysTick->VAL    =   21;                         //Clears with any value
    SysTick->CTRL   =   BIT0;                       //Enables Clock
    while((SysTick->CTRL & BIT(16))==0);            //wait for delay time to elapse
}
//This functions accepts an amount of milliseconds and calls a function to delay
//the given amount of milliseconds desired
void delayMS(uint8_t delay){
    SysTick->LOAD = (delay * 3000 - 1);            //3000 clocks = 1 milii
    SysTick->VAL = 0;                           //puts time to zero
    while((SysTick->CTRL & BIT(16))==0);       //wait for delay time to elapse
}
//This functions accepts an amount of microseconds and calls a function to delay
//the given amount of microseconds desired
void delayUS(uint16_t delay){
    SysTick->LOAD = (delay * 3 - 1);            //3 = clocks = 1 micro
    SysTick->VAL = 0;                           //puts time to zero
    while((SysTick->CTRL & BIT(16))==0);       //wait for delay time to elapse
}
//Sets up a PWM using systick delays
void SysTickPWM(){

    P2->SEL0    |=  BIT4;                                           //GPIO for PWM
    P2->SEL0    &=  ~BIT4;
    P2->DIR     |=  BIT4;                                           //OUTPUT

    float dutyCycle     = .9;                                       //%duty cycle
    uint16_t period     = 25000;                                    //period of 25 milliseconds = 40Hz frequency
    uint16_t ON_time    = period * dutyCycle;                       //period of 25 milliseconds = 40Hz frequency
    uint16_t OFF_time   = period * (1 - dutyCycle);                 //period of 25 milliseconds = 40Hz frequency

    while(1){
    P2->OUT     |= BIT4;                                            //P2.4 ON
    delayUS(ON_time);                                               //delay
    P2->OUT     &= ~BIT4;                                           //P2.4 OFF
    delayUS((uint16_t)OFF_time);                                    //delay
    }
}
