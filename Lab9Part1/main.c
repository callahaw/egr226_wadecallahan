/**
 *Wade Callahan
 *Prof Ekin
 *EGR 226 Lab 9
 *This program will change the duty Cycle of a PWM signal based
 *on the user's input from three external buttons. One button increases
 *the duty cycle by 10, one decreases the duty cycle by 10, and the third sets the
 *duty cycle to zero
 */

#include "msp.h"

//functionn prototype
void pwminit(uint16_t period);
void  SysTick_Init();
void delayMS(int delay);

//global variables
int dutyCycle = 20;
uint16_t period = 60000;                                                            //Period, 60000 cycle = 20 ms = 50Hz frequency


void main(void)
{
    __disable_irq();

    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;                                         // stop watchdog timer


    SysTick_Init();                                                                     //init systick timer
    pwminit(period);                                                                    //function call to pwm



        while (1);
}

//This function is used to generate a PWM using TimerA
//The function accepts a period and a duty cycle as its input
//The function setsup the pins associatied with TA0.1
void pwminit(uint16_t period){

    P2->SEL0            |=  BIT4;                                                       //GPIO for PWM
    P2->SEL1            &=  ~BIT4;
    P2->DIR             =   BIT4;                                                       //OUTPUt

    P2->IES     |=  BIT5|BIT6|BIT7;                                                     //Sets up interupt
    P2->IE      |=  BIT5|BIT6|BIT7;
    P2->IFG = 0;                                                                        //clears

    NVIC_EnableIRQ(PORT2_IRQn);                                                         //enable port interupt
    __enable_irq();

    TIMER_A0->CTL       =   TIMER_A_CTL_SSEL__SMCLK |                                   // SMCLK
                            TIMER_A_CTL_ID_3        |                                   // divide by 8
                            TIMER_A_CTL_MC_1;                                           // Up mode

    TIMER_A0->CCR[0]    = period;                                                       //Loads the period
    TIMER_A0->CCTL[1]   = 0b11100000;                                                   //reet/set

    while(1)
        TIMER_A0->CCR[1]    = (period * dutyCycle / 100);                               //loads the On time continously for debugging purposes

}
//Initializes the systick timer
void  SysTick_Init(){
    SysTick->LOAD   =   3000 - 1;                   //1 Millisecond
    SysTick->VAL    =   21;                         //Clears with any value
    SysTick->CTRL   =   BIT0;                       //Enables Clock
    while((SysTick->CTRL & BIT(16))==0);            //wait for delay time to elapse
}
//This functions accepts an amount of milliseconds and calls a function to delay
//the given amount of milliseconds desired
void delayMS(int delay){
    SysTick->LOAD = (delay * 3000 - 1);             //3000 clocks = 1 milii
    SysTick->VAL = 0;                               //puts time to zero
    while((SysTick->CTRL & BIT(16))==0);            //wait for delay time to elapse
}

//Port 2 Interupt handler
//Checks button and changes duty cycle
void PORT2_IRQHandler(void){
    if(P2->IFG & BIT6 ){                            //Checks 2.6
        delayMS(50);                                //kind of debounces
        if(dutyCycle >= 0)                          //Don't be less than 0
            dutyCycle -= 10;                        //changes duty cycle
    }
    if(P2->IFG & BIT7 ){                            //2.7
        delayMS(50);                                //debounces
        if(dutyCycle <= 100)                        //can't have duty cycle bigger than 100%
            dutyCycle += 10;                        //changes duty cycle
    }
    if(P2->IFG & BIT5 ){                            //2.5
        dutyCycle = 0;                              //stops motor
    }
    P2->IFG &= ~BIT6;                               //clears
    P2->IFG &= ~BIT7;                               //clears
    P2->IFG &= ~BIT5;                               //clears
    P2->IFG = 0;                                    //clears
}

