/**
 * Wade Callahan
 * Prof Ekin
 * Lab4 PART 1
 * This part of the lab requires the used to be able to
 * flash an LED with the push of a button. The LED will start in the off
 * Position. The user should hold the button. The LED should change every
 * 1 second. once the button is let go the program will stay in the
 * same led state
 */

//LIBRARIES
#include "msp.h"
#include <stdio.h>

//FUNCTION PROTOTYPES
int button_Debounce(int delay);
void delay(int delay);

void main(void)
{

WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD; // stop watchdog timer

    //GPIO Setup
    //Setup for a button that connects to ground
    P1->SEL0    &=  ~BIT1;                       //gpio P1.1
    P1->SEL1    &=  ~BIT1;                       //GPIO P1.1
    P1->DIR     &=  ~BIT1;                       //direction is input (0)
    P1->REN     |=  BIT1;                        //enable resistor
    P1->OUT     |=  BIT1;                        //set default state to a 1

    //Red Setup
    P2->SEL0    &=  ~BIT0;                      //GPIO for P2.0, SEL = 00
    P2->SEL1    &=  ~BIT0;                      //GPIO
    P2->DIR     |=  BIT0;                       //Set P2.0 as output
    P2->OUT     &=  ~BIT0;                      //Sets output to off


    //Green Setup
    P2->SEL0    &=  ~BIT1;                      //configure P2.1 as GPIO
    P2->SEL1    &=  ~BIT1;                      //configure P2.1 as GPIO
    P2->DIR     |=  BIT1;                       //make P2.1 output
    P2->OUT     &=  ~BIT1;                      //2.1 set to off

    //Blue Setup
    P2->SEL0    &=  ~BIT2;                      //configure 2.2 as GPIO
    P2->SEL1    &=  ~BIT2;                      //configure P2.2 as GPIO
    P2->DIR     |=  BIT2;                       //make 2.2 output
    P2->OUT     &=  ~BIT2;                      //P2.2 set off


    int timeDelay = 70000;                      //used for time delay

    int buttonState = 0;                        //used to determine which color should be on

        //Infinite Loop
        while(1)
        {
            //This loop is used to keep the LED in its appropriate state
            while(button_Debounce(timeDelay))
            {
                //State for RED LED
                if(buttonState == 0)
                {
                    P2->OUT     |= BIT0;                                //red ON
                    P2->OUT     &= ~BIT2;                               //blue OFF
                    P2->OUT     &= ~BIT1;                               //green OFF
                    delay(timeDelay);                                   //Delay before changing state
                    if (!button_Debounce(timeDelay))break;              //If button is not pressed, break out of this loop
                    buttonState ++;
                }
                //State for Green LED
                else if (buttonState == 1)
                {
                    P2->OUT     |= BIT1;                                    //green ON
                    P2->OUT     &= ~BIT0;                                   //red OFF
                    P2->OUT     &= ~BIT2;                                   //blue OFF
                    delay(timeDelay);
                    if (!button_Debounce(timeDelay))break;
                    buttonState ++;
                }
                //State for BLUE LED
                else if (buttonState == 2)
                {
                    P2->OUT     |= BIT2;                                    //blue ON
                    P2->OUT     &= ~BIT1;                                   //green OFF
                    P2->OUT     &= ~BIT0;                                   //red OFF
                    delay(timeDelay);
                    if (!button_Debounce(timeDelay))break;
                    buttonState = 0;
                }
            }
        }
}



//FUNCTION DEFINITIONS

//Function is used to delay the program
//The input for this is an integer delay
//Function will be called in button debounce function
void delay(int delay){
    int i;
    for(i = 0; i < delay; i++)
        __delay_cycles(1);                          //Uses delay cycle of 1 each loop through
}


//This function is used to ensure a button press is read in correctly
//The function will first check if the button is pressed.
//It will then delay for some time and check if the button is still pressed
//If it is, it will return 1, if not, it will return 0
int button_Debounce(int delayTime){
    if(!(P1->IN & BIT1)){                           //checks if the button is pressed
            delay(delayTime);                       //delay the time
                if(!(P1->IN & BIT1))                //Checks if button is pressed again
                        return 1;                   //returns one if button was pressed
    }
    return 0;                                       //returns 0 if button is not pressed
}
