/*
 *Wade Callahan
 *Prof. Ekin
 *EGR 226 LAB
 *LAB_1_StatsLib
 *Description: This lab is intended to be a refresher for structures in C. In this lab,
 *a structure will be created and used to store information from a book database.
 *A parsefile algorithm will be used to gather the databse information from an excel spreadsheet.
 *Users can search this database by title, author, or ISBN.
 */
 //Macros
#define NULL 0
#define NUM_BOOKS 360
//Library
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#include <string.h>

//structure definition
 typedef struct {
     char title[255];
     char author_name[50];
     char ISBN[10];
     int pages;
     int year_published;
 } book;

 //function prototypes
int parse_file(char fileName[], book bookArr[]);
void print_book(book *bookVar);
void search_title(book bookArr[], int n, char title[]);
void search_author(book bookArr[], int n, char author_name[]);
void search_ISBN(book bookArr[], int n, char ISBN[]);

int main(){
    //Variable, strings and structures declared
    char fileName[25];
    int num = 360;
    book my_book_array[num];
    char title[50];
    char author[50];
    char ISBN[50];
    int choice, error; //Variable declaration

    //Input name of file here
    strcpy(fileName, "BookList.csv");


    parse_file(fileName, my_book_array);                                    //function call to parse file


    while(1){

        //prompts user for choice
        printf("Choose an option :\n");
        printf("[0] Search by Title \n");
        printf("[1] Search by Auther Name \n");
        printf("[2] Search by ISBN \n");
        printf("[3] Exit \n\n\n");


        do{
            fflush(stdin);                                                                  //flushes input
            error = scanf("%d", &choice);
                //Yells at user if input is not valid
                if( error == 0 || choice < 0 || choice > 3)
                    printf("Please Enter a VALID input! \n\n");

        }while( error == 0 || choice < 0 || choice > 3);                                //Loops until valid input


        switch(choice){
        case 0:
            printf("Please Enter a title!\n");                                           //prompts user to enter a title
            fflush(stdin);
            gets(title);
            search_title(my_book_array, num, title);
            break;
        case 1:
            printf("Please Enter an author!\n");                                            //prompts user to enter an author
            fflush(stdin);
            gets(author);
            search_author(my_book_array, num, author);
            break;
        case 2:
            printf("Please Enter an ISBN!\n");                                              //prompts user to enter an ISBN
            fflush(stdin);
            gets(ISBN);
            search_ISBN(my_book_array, num, ISBN);
            break;
        case 3:
            break;
        }if(choice == 3)break;                                                           //Breaks loop only when 3 is entered
    }


    return 0;
}

int parse_file(char fileName[], book bookArr[]){
    FILE *inFile = fopen(fileName, "r");                //Tries to open file

    if(inFile == NULL)                                  //Returns 0 if file could not open
        return 0;

    char buffer[512];                                   //Temporary string buffer
    int i = 0;                                          //Used to index bookArr

    //The following code was referenced and modified from Prof. Ekin's Video Demonstration
    while(fgets(buffer, 512, inFile)){                  //Collects a line until no more lines are left
        char *ptr = strtok(buffer, ",");                //Parse string by commas
        if(strcmp(ptr, "N/A"))                          //Validates string
            strcpy(bookArr[i].title, ptr);              //Parse for book title

        ptr = strtok(NULL, ",\n");
        if(strcmp(ptr, "N/A"))                          //Validates string
            strcpy(bookArr[i].author_name, ptr);        //Parse for author name

        ptr = strtok(NULL, ",\n");
        if(strcmp(ptr, "N/A"))                          //Validates string
            strcpy(bookArr[i].ISBN, ptr);               //Parse for ISBN

        ptr = strtok(NULL, ",\n");
        if(strcmp(ptr, "N/A"))                          //Validates string
            bookArr[i].pages = atoi(ptr);                 //Parse for ISBN

        ptr = strtok(NULL, ",\n");
        if(strcmp(ptr, "N/A"))                          //Validates string
            bookArr[i].year_published = atoi(ptr);      //Parse for ISBN

        i++;
    }
    return 1;
}
//This function will print the information for any given book input
void print_book(book *bookVar){

    printf("|Title: %s\n", bookVar->title);

    printf("|Author: %s\n", bookVar->author_name);

    if(atoi(bookVar->ISBN) == 0)                                //atoi returns NULL if letter instead of integer
        printf("|ISBN: N/A\n");
    else printf("|ISBN: %s\n", bookVar->ISBN);

    if(bookVar->pages == 0)
        printf("|Pages: N/A\n");
    else printf("|Pages: %d\n", bookVar->pages);

    if(bookVar->year_published == 0)
        printf("|Year Published: N/A\n\n\n");
    else printf("|Year Published: %d\n\n\n", bookVar->year_published);
}
//
void search_title(book bookArr[], int n, char title[]){
    int i;
    for(i = 0; i < n; i++){                                                     //Loops through every book in array
        if(strstr(strlwr(bookArr[i].title), strlwr(title)))                                     //strstr returns NULL if substring is not in string
            print_book(&bookArr[i]);                                            //function call to print book info
    }
}

void search_author(book bookArr[], int n, char author_name[]){
    int i;
    for(i = 0; i < n; i++){                                                                 //Loops through every book in array
        if(strstr(strlwr(bookArr[i].author_name), strlwr(author_name))!=0)                  //strstr returns NULL if substring is not in string, considers both strings in lower case
            print_book(&bookArr[i]);                                                        //function call to print book info
    }
}

void search_ISBN(book bookArr[], int n, char ISBN[]){
    int i;
    for(i = 0; i < n; i++){                                                                  //Loops through every book in array
        if(strstr((bookArr[i].ISBN), (ISBN))!=0)                                            //strstr returns NULL if substring is not in string
            print_book(&bookArr[i]);                                                        //function call to print book info
    }
}
