
/**
 * Wade Callahan
 * Prof. Ekin
 * Lab5Part 2
 * This function will read input from an external button and sequence three external LEDs
 * SysTick timee will be used in this program. if the user presses the button the led should
 * change. if the user hold the button down the led should not change states
 */

//libraries
#include "msp.h"
#include <stdio.h>

//function prototypes
int button_Debounce(uint16_t delay);
void  SysTick_Init();
void SysTick_Delay(uint16_t delayms);

void main(void)
{


	WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;		// stop watchdog timer


        //GREEN LED
        P3->SEL0    &=  ~BIT2;                      //configure 3.2 as GPIO
        P3->SEL1    &=  ~BIT2;                      //configure P2.2 as GPIO
        P3->DIR     |=  BIT2;                       //make 23.2 output
        P3->OUT     &=  ~BIT2;                      //P3.2 et off
        //YELLOW LED
        P3->SEL0    &=  ~BIT3;                      //configure 3.3 as GPIO
        P3->SEL1    &=  ~BIT3;                      //configure P3.3 as GPIO
        P3->DIR     |=  BIT3;                       //make 3.3 output
        P3->OUT     &=  ~BIT3;                      //P3.3 set off
        //RED LED
        P4->SEL0    &=  ~BIT3;                      //configure 4.3 as GPIO
        P4->SEL1    &=  ~BIT3;                      //configure P4.3 as GPIO
        P4->DIR     |=  BIT3;                       //make 4.3 output
        P4->OUT     &=  ~BIT3;                      //P4.3 set off
        //button 1
        P2->SEL0    &=  ~BIT7;                       //gpio P2.7
        P2->SEL1    &=  ~BIT7;                       //GPIO P2.7
        P2->DIR     &=  ~BIT7;                       //direction is input (0)
        P2->REN     |=  BIT7;                        //Enable resistor
        P2->OUT     |=  BIT7;                        //set default state to a 1

	    uint16_t timeDelay = 5;                      //used for time delay



	    //Main loop
	    while(1){

	        while (!button_Debounce(timeDelay));                  //stays in loop until button is pressed
	        P3->OUT       |= BIT2;                                //green is turned ON
	        P4->OUT       &= ~BIT3;                               //red is turned OFF
	        while (button_Debounce(timeDelay));                   //Used to make sure LED doesnt change while button is held down


	        while (!button_Debounce(timeDelay));
	        P3->OUT       |= BIT3;                                //yellow is turned ON
	        P3->OUT       &= ~BIT2;                               //green is turned OFF
	        while (button_Debounce(timeDelay));


	        while (!button_Debounce(timeDelay));
	        P4->OUT       |= BIT3;                                //red is Turned ON
	        P3->OUT       &= ~BIT3;                                //yellow is turned OFF
	        while (button_Debounce(timeDelay));

	    }
}

//This function initializes a SysTick timer and sets it to run for 1 millisecond
void  SysTick_Init(){
    SysTick->LOAD   =   3000 - 1;               //1 Millisecond
    SysTick->VAL    =   21;                     //Clears with any value
    SysTick->CTRL   =   BIT0;                   //Enables Clock
    while(1){
        if(SysTick->CTRL&BIT(16))               //Delays until Clock ends
            break;
    }
}
//This functions accepts an amount of milliseconds and calls a function to delay
//the given amount of milliseconds desired
void SysTick_Delay(uint16_t delayms){
    int i;
    for(i = 0; i <= delayms; i++){
        SysTick_Init();
    }
}
//this function is used to debounce the button. it will first check if the button is pressed
// it will then delay and check the buttons state again
int button_Debounce(uint16_t delayTime){
    if(!(P2->IN & BIT7)){                                   //checks if the button is pressed
            SysTick_Delay(delayTime);                       //delay the time
                if(!(P2->IN & BIT7)){                        //Checks if button is pressed again
                        return 1;                           //returns one if button was pressed
                }
    }
    return 0;                                               //returns 0 if button is not pressed
}
