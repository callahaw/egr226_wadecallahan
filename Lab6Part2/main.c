/**********************************
 * Wade Callahan
 * Prof. Ekin
 * EGR 226 LAB 6
 *This program wil accept user input from a keypad and print to console
 *The program should read input from the keypad and store it into an array
 *after the pound sign is pressed the program should print the contents of
 *the array to the console.
 *********************************/

//LIBRARIES USED
#include "msp.h"
#include <stdio.h>


//MACROS for Keypad
#define ASTERIK 19
#define POUND 21
#define START 657
#define ENTRIES 4

//function prototypes
void keypadSetup();
void Read_Keypad(void);
void storeKey();
void printKeys(int pinCode[]);
int button_Debounce(uint16_t delay, int bit);
void  SysTick_Init();
void SysTick_Delay(uint16_t delayms);

//global variable
int keyPressed = -1;
int num = 0;

void main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;     // stop watchdog timer

    keypadSetup();
    printf("Please enter a key on the keypad!\n");
    while(1){
        storeKey();
    }
}

//this function sets up the keypad
void keypadSetup(){
    P4->SEL0    &= ~(BIT0|BIT1|BIT2|BIT3|BIT4|BIT5|BIT6);
    P4->SEL1    &= ~(BIT0|BIT1|BIT2|BIT3|BIT4|BIT5|BIT6);
    P4->DIR     &= ~(BIT0|BIT1|BIT2|BIT3|BIT4|BIT5|BIT6);
    P4->REN     |=  (BIT0|BIT1|BIT2|BIT3|BIT4|BIT5|BIT6);
    P4->OUT     |=  (BIT0|BIT1|BIT2|BIT3|BIT4|BIT5|BIT6);
}
//This function will read in values from the keypad and store
//them in a global variable
void Read_Keypad(void)
{
      int bit = 0b00000001;
      uint16_t debounceDelay = 5;

      //COLUMN 1 KEYS
      P4->OUT   &= ~BIT4;                                               //Set default state to a 0
      P4->DIR   |=  BIT4;                                               //Change to an output
      while(button_Debounce(debounceDelay   ,   bit))                  //Checks if key 1 is press
         keyPressed     =   1;
      bit<<=1;                                                          //shifts bit over 1 spot

      while(button_Debounce(debounceDelay   ,   bit))                   //check if P4.1 is active
          keyPressed    =   4;
      bit<<=1;

      while(button_Debounce(debounceDelay   ,   bit))                   //checks if p4.2 is active
          keyPressed    =   7;
      bit<<=1;

      while(button_Debounce(debounceDelay   ,   bit))                   //checks if P4.3 is active
         keyPressed     =   ASTERIK;
      P4->DIR   &= ~BIT4;                                               //Change to an input
      P4->OUT   |=  BIT4;                                               //Set default state to a 1
      bit = 0b00000001;                                                 //resets bit

      //COLUMN 2 KEYS
      P4->OUT   &= ~BIT5;                   //Set default state to a 0
      P4->DIR   |=  BIT5;                   //Change to an output (that is 0 due to previous line)
      while(button_Debounce(debounceDelay   ,   bit))
          keyPressed    =   2;
      bit<<=1;

      while(button_Debounce(debounceDelay   ,   bit))
          keyPressed    =   5;
      bit<<=1;

      while(button_Debounce(debounceDelay   ,   bit))
          keyPressed    =   8;
      bit<<=1;

      while(button_Debounce(debounceDelay   ,   bit))
          keyPressed    =   0;

      P4->DIR   &= ~BIT5;                   //Change to an input
      P4->OUT   |=  BIT5;                   //Set default state to a 1 (pull up)
      bit = 0b00000001;

      //COLUMN 3 KEYS
      P4->OUT   &= ~BIT6;                   //Set default state to a 0
      P4->DIR   |=  BIT6;                   //Change to an output (that is 0 due to previous line)

      while(button_Debounce(debounceDelay   ,   bit))
          keyPressed    =   3;
      bit<<=1;

      while(button_Debounce(debounceDelay   ,   bit))
          keyPressed    =   6;
      bit<<=1;

      while(button_Debounce(debounceDelay   ,   bit))
          keyPressed    =   9;
      bit<<=1;

      while(button_Debounce(debounceDelay   ,   bit))
          keyPressed    =   POUND;
      P4->DIR   &= ~BIT6;                   //Change to an input
      P4->OUT   |=  BIT6;                   //Set default state to a 1 (pull up)
      bit = 0b00000001;
}

//This function is used to store the keys pressed in an array
//the function will also call a function to print the values in the array
void storeKey(){
    int i;                                                      //loop management
    int pin[ENTRIES];                                           //declares an array
        for(i = 0; i < ENTRIES + 1; ++i){
            while(keyPressed == -1)                             //waits here until a key is read
                Read_Keypad();
            //switch case for keys pressed
            switch(keyPressed){
            case 0:
                printf("0\n");                                  //prints key
                pin[i] = keyPressed;                            //puts keyPressed in the array[i]
                keyPressed = -1;                                //resets key back to -1
                break;
            case 1:
                printf("1\n");
                pin[i] = keyPressed;
                keyPressed = -1;
                break;
            case 2:
                printf("2\n");
                pin[i] = keyPressed;
                keyPressed = -1;
                break;
            case 3:
                printf("3\n");
                pin[i] = keyPressed;
                keyPressed = -1;
                break;
            case 4:
                printf("4\n");
                pin[i] = keyPressed;
                keyPressed = -1;
                break;
            case 5:
                printf("5\n");
                pin[i] = keyPressed;
                keyPressed = -1;
                break;
            case 6:
                printf("6\n");
                pin[i] = keyPressed;
                keyPressed = -1;
                break;
            case 7:
                printf("7\n");
                pin[i] = keyPressed;
                keyPressed = -1;
                break;
            case 8:
                printf("8\n");
                pin[i] = keyPressed;
                keyPressed = -1;
                break;
            case 9:
                printf("9\n");
                pin[i] = keyPressed;
                keyPressed = -1;
                break;
            case ASTERIK:
                printf("Please enter a valid input!\n");                //error message for asteriks
                i = -1;                                                 //resets the i to -1 (i increments to 0 after switch
                keyPressed = -1;
                break;
            case POUND:
                if(num <= 3){                                           //checks if pound is pressed before three values input
                    printf("Please enter a valid input!\n");            //error statement
                    i = -1;                                             //resets i
                }
                else
                    printKeys(pin);                                     //prints the pin code upon successful input
                keyPressed = -1;                                        //resets i
                break;
           }
            num++;
       }
    }

//This function will print the pincode to the console.
//it accepts an array as its input. it will also reset
//the value of num back to 0
void printKeys(int pinNum[]){
    int i;
    printf("Your code is:\n");
    for(i = 0; i < ENTRIES; i++){
        printf("%d\n", pinNum[i]);                              //prints each value of the array
    }
    keyPressed = START;                                         //resets keyPressed back to start
    printf("Please enter a key on the keypad!\n");              //prompts user to enter a new code
    num = 0;                                                    //resets the value of num
}

//This function initializes a SysTick timer and sets it to run for 1 millisecond
void  SysTick_Init(){
    SysTick->LOAD   =   3000 - 1;               //1 Millisecond
    SysTick->VAL    =   21;                     //Clears with any value
    SysTick->CTRL   =   BIT0;                   //Enables Clock
    while(1){
        if(SysTick->CTRL&BIT(16))               //Delays until Clock ends
            break;
    }
}
//This functions accepts an amount of milliseconds and calls a function to delay
//the given amount of milliseconds desired
void SysTick_Delay(uint16_t delayms){
    int i;
    for(i = 0; i <= delayms; i++){
        SysTick_Init();
    }
}
//this function is used to debounce the button. it will first check if the button is pressed
// it will then delay and check the buttons state again
int button_Debounce(uint16_t delayTime, int bit){
    if(!(P4->IN & bit)){                                   //checks if the button is pressed
            SysTick_Delay(delayTime);                       //delay the time
                if(!(P4->IN & bit)){                        //Checks if button is pressed again
                        return 1;                           //returns one if button was pressed
                }
    }
    return 0;                                               //returns 0 if button is not pressed
}
