/*
 * Wade Callahan
 *Prof. Ekin
 * EGR 226 Lab
 * Description: This program is a dice game. A user inputs the amount of credits they want to start with.
 *the user will then choose the amount they want to bet. The program will generate to random numbers,one
 *for the computer and the other for the user. The winner will be the person who has the higher number
 *The user will then be prompted if they want to play again
 *IMPORTANT NOTE: Program uses fflush to clear input. fflush does not work on code composer
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>

//function prototypes
int inputCredits();

int placeBet(int c);

int playGame();

int main(){
    //variable declaration
    int credits, bet, results;
    int choice;
    //used to generate the random numbers
    srand(time(NULL));

    printf("Welcome to the Dice Game!\n\n");
    credits = inputCredits();

    //cycles until player chooses to end game
    do{
    //place bet
    bet = placeBet(credits);
    results = playGame();

    //function playGame() returns 0 if user loser
    //and return 1 if player wins
    if (results == 0)credits-=bet;
    else if (results == 1)credits+=bet;


    //allows user to continue only if they have more than 0 credits
    if(credits > 0){
    printf("You have %d credits.\n Press 1 to play again.\n", credits);
    scanf("%d", &choice);
    }else break;
    }while(choice == 1);

    //tells user their credits at end of game
    printf("You finished with %d credits.", credits);
    return 0;
}
//this function is used to prompt user to enter starting credits
//error checking is used to make sure proper entry
//return the credit amount
int inputCredits(){
    int credits, error; //Variable declaration

    do{
        printf("Please enter credits desired:\n"); //Prompts User for Input
        error = scanf("%d", &credits);
        fflush(stdin);

            //Yells at user if input is not valid
            if( error == 0 || credits < 0)printf("Please Enter a VALID input! ");

    }while( error == 0 || credits < 0);  //Loops until valid input
    printf("Your credit amount is %d\n", credits);
    return credits;
}


//this function is used to prompt user to place a bet
//error checking is used to make sure proper entry
//return the bet amount
int placeBet(int c){
    int bet, error; //Variable declaration

    do{
        printf("Enter your bet:\n"); //Prompts User for Input
        error = scanf("%d", &bet);
        fflush(stdin);//flushes input

            //Yells at user if input is not valid
            if( error == 0 || (bet > c || bet < 0))printf("Please Enter a VALID input! ");

    }while( error == 0 || (bet > c || bet < 0));  //Loops until valid input
    printf("Your bet amount is %d\n", bet);
    return bet;
}


//This function will generate two random numbers
//One number for the computer and the other for the user
//the high number wins. the functions return 0,1,or 2 based on the results
int playGame(){
    int compRoll, userRoll;
    //generates any random number and uses %6 to get number 0-5
    //add one to get 1-6
    compRoll = (rand()%6) + 1;
    userRoll = (rand()%6) + 1;

    printf("Computer rolled: %d\n You rolled: %d\n", compRoll, userRoll);
    //determines the winner and returns the results
    if(compRoll > userRoll){
        printf("You lost!\n");
        return 0;
    }else if(compRoll < userRoll){
        printf("You won!\n");
        return 1;
    }else{
        printf("You tied!\n");
        return 2;
    }
}
