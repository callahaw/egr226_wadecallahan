/*Wade Callahan
 * Prof Ekin
 * EGR 226 Lab 7
 * This program will initialize a cursor on the LCD.
 * Upon a push of the button at p1.4, the program will go into part
 * 2 of the lab and display first and last name "EGR" and 226 all on
 * their own lines
 */


/** PINS USED:
 * DB4-7    ->  P4.4-P4.7
 * E        ->  P4.2
 * RW       ->  P4.1
 * RS       ->  P4.3
 */

//Libraries
#include "msp.h"
#include <string.h>

//Function prototypes
void delay_us(int us);
void  SysTick_Init();
void delayMS(uint8_t delay);
void delayUS(uint8_t delay);
void cmdWrite(uint8_t command);
void pulseEN();
void pushNibble(uint8_t nibble);
void pushByte(uint8_t byte);
void dataWrite(uint8_t data);
void LCD_Init();
void writeString(char string[]);
void printLCD(void);


void main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;     // stop watchdog timer


    SysTick_Init();                                 //initialize the systick timer

    LCD_Init();                                     //Part 1, initilizes the lcd, blinks cursor

    //Sets up on board button, used to go from part 1 to part 2, no debounce used
    P1->SEL0    &=  ~BIT4;                          //gpio P1.1
    P1->SEL1    &=  ~BIT4;                          //GPIO P1.1
    P1->DIR     &=  ~BIT4;                          //direction is input (0)
    P1->REN     |=  BIT4;
    P1->OUT     |=  BIT4;                           //set default state to a 1

    while((P1->IN & BIT4));                         //Stays in part 1 until button is pressed

    printLCD();                                     //Part 2, prints info to screen

    //Nothing goes gets looped :(
    while(1){
    }
}

//This function initializes a SysTick timer and sets it to run for 1 millisecond
void  SysTick_Init(){
    SysTick->LOAD   =   3000 - 1;                   //1 Millisecond
    SysTick->VAL    =   21;                         //Clears with any value
    SysTick->CTRL   =   BIT0;                       //Enables Clock
    while((SysTick->CTRL & BIT(16))==0);            //wait for delay time to elapse
}
//This functions accepts an amount of milliseconds and calls a function to delay
//the given amount of milliseconds desired
void delayMS(uint8_t delay){
    SysTick->LOAD = (delay * 3000 - 1);            //3000 clocks = 1 milii
    SysTick->VAL = 0;                           //puts time to zero
    while((SysTick->CTRL & BIT(16))==0);       //wait for delay time to elapse
}
//This functions accepts an amount of microseconds and calls a function to delay
//the given amount of microseconds desired
void delayUS(uint8_t delay){
    SysTick->LOAD = (delay * 3 - 1);            //3 = clocks = 1 micro
    SysTick->VAL = 0;                           //puts time to zero
    while((SysTick->CTRL & BIT(16))==0);       //wait for delay time to elapse
}
//Function is used to write a command to the LCD, RS is set to zero
void cmdWrite(uint8_t command){
    P4->OUT     &= ~BIT3;                       //send command, RS = 0
    delayMS(1);
    pushByte(command);                          //calls pushByte
}
//Function is used to write data to the LCD, RS is set to one
void dataWrite(uint8_t data){
    P4->OUT     |= BIT3;                        //send Data, RS = 1
    delayMS(1);
    pushByte(data);
}
//Calls push nibble, used to seperate the two nibbles in a byte so that
//they can be sent to the scenn.
void pushByte(uint8_t byte){
    int top, bottom;
    top = (byte & 0xF0) >> 4;                       //sets to the top four bits
    pushNibble(top);                                //call pushNibble
    delayMS(1);                                     //delays 1 milli
    bottom = byte & 0x0F;                           //sets to the bottom four bits
    pushNibble(bottom);
    delayMS(1);
}
//Push a nibble to screen, clears top four bit in output,
//Pushes desired nibble to the screen, pulse enable
void pushNibble(uint8_t nibble){
    P4->OUT &= ~0xF0;
    P4->OUT |= (nibble & 0x0F) << 4;
    pulseEN();
}

//Pulses the enable pin (P4,2), first sets low,
//delays, drives high, delay, and drive low again
//used to send command or data
void pulseEN(){
    P4->OUT     &= ~BIT2; //Enable Low
    delayUS(10);
    P4->OUT     |= BIT2; //Enable High
    delayUS(10);
    P4->OUT     &= ~BIT2; //Enable Low
    delayUS(10);
}
//Initializes the LCD, DONT CHANGE THE DELAYS
//PINS SETUP in here
void LCD_Init(){
/** PINS USED:
 * DB4-7 -> P4.4-P4.7
 * E -> P4.2
 * RW -> P4.1
 * RS -> P4.3
 */

    P4->SEL0 &= ~(0xFE); //(0b11111110)
    P4->SEL1 &= ~(0xFE); //GPIO
    P4->DIR  |=  (0xFE); //Output
    P4->OUT  &= ~(0xFE); //Initially zero (including RW)

    delayMS(60);

    cmdWrite(0x03);                                 // reset sequence, do three times, dont change delays!
    delayMS(10);
    cmdWrite(0x03);
    delayUS(200);
    cmdWrite(0x03);
    delayMS(10);

    cmdWrite(0x02);                                 // setting 4 bit mode
    delayUS(100);

    cmdWrite(0x08);                                 // 5x7 Format
    delayUS(100);

    cmdWrite(0x0F);                                 // 0x0F = blinking cursor or 0x0C = no crusor
    delayUS(100);

    cmdWrite(0x01);                                 // clear display, move cursor home
    delayUS(100);

    cmdWrite(0x06);                                 // increment cursor
    delayUS(100);
}

//Function accepts a scrint and writes the data to the LCD one character at a time
void writeString(char string[]){
    int i;
    for(i=0;    i<strlen(string);   i++)        //loop for the length of the string
    {
        dataWrite(string[i]);
    }
}

void printLCD(void){
    //Strings to be printed
    char *str1 = "Wade";
    char *str2 = "Callahan";
    char *str3 = "EGR";
    char *str4 = "226";

    cmdWrite(1);                                //clear display
    delayMS(1);

    cmdWrite(0x86);                             //put to line address at line 1 spot 07 (0x80 + 0x06)
    writeString(str1);

    cmdWrite(0xC4);                             //put to line 2
    writeString(str2);

    cmdWrite(0x96);                             //put to line 3
    writeString(str3);

    cmdWrite(0xD6);                             //put to line 4
    writeString(str4);
}
