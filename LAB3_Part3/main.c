#include "msp.h"


/**
 * Wade Callahan
 * Prof Ekin
 * Lab3 MSP432 Introduction
 * This lab is intended to flash an led ON/OFf by a variable amount of time
 */


void main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;     // stop watchdog timer
    //Configue GPIO for output on P1.0LED
    P1->DIR = BIT0;

    int ON_delayTime = 20000;                                  //Delay Variable to change blink rate
    int OFF_delayTime = 20000;                                  //Delay Variable to change blink rate

    int i;                                          //Declare loop variable
    while(1){
        P1->OUT ^= BIT0;                            //Inverts LED output with each loop through
        for(i = ON_delayTime; i > 0; i--);             //Delays the time between on and off
        P1->OUT ^= BIT0;                            //Inverts LED output with each loop through
        for(i = OFF_delayTime; i > 0; i--);             //Delays the time between on and off
    }
}




