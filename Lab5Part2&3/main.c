
/**
 * Wade Callahan
 * Prof. Ekin
 * Lab5Part 2&3
 * This function will read input from an external button and sequence three external LEDs
 * SysTick timee will be used in this program
 */

//libraries
#include "msp.h"
#include <stdio.h>

//Function Prototypes
int button1_Debounce(uint16_t delay);
int button2_Debounce(uint16_t delay);
void  SysTick_Init();
void SysTick_Delay(uint16_t delayms);

//Enum for the LED states
enum STATES{
    OFF,
    GREEN,
    YELLOW,
    RED
};

void main(void)
 {
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;     // stop watchdog timer

            enum STATES state;
            state = OFF;                                //Sets state to OFF

            //GREEN LED
            P3->SEL0    &=  ~BIT2;                      //configure 3.2 as GPIO
            P3->SEL1    &=  ~BIT2;                      //configure P2.2 as GPIO
            P3->DIR     |=  BIT2;                       //make 23.2 output
            P3->OUT     &=  ~BIT2;                      //P3.2 et off
            //YELLOW LED
            P3->SEL0    &=  ~BIT3;                      //configure 3.3 as GPIO
            P3->SEL1    &=  ~BIT3;                      //configure P3.3 as GPIO
            P3->DIR     |=  BIT3;                       //make 3.3 output
            P3->OUT     &=  ~BIT3;                      //P3.3 set off
            //RED LED
            P4->SEL0    &=  ~BIT3;                      //configure 4.3 as GPIO
            P4->SEL1    &=  ~BIT3;                      //configure P4.3 as GPIO
            P4->DIR     |=  BIT3;                       //make 4.3 output
            P4->OUT     &=  ~BIT3;                      //P4.3 set off
            //button 1
            P2->SEL0    &=  ~BIT7;                       //gpio P2.7
            P2->SEL1    &=  ~BIT7;                       //GPIO P2.7
            P2->DIR     &=  ~BIT7;                       //direction is input (0)
            P2->REN     |=  BIT7;                        //Enable resistor
            P2->OUT     |=  BIT7;                        //set default state to a 1
            //button 2
            P2->SEL0    &=  ~BIT6;                       //gpio P2.6
            P2->SEL1    &=  ~BIT6;
            P2->DIR     &=  ~BIT6;                       //direction is input (0)
            P2->REN     |=  BIT6;                        //enable resistor
            P2->OUT     |=  BIT6;                        //set default state to a 1

            uint16_t timeDelay = 1000;                      //used for time delay
            uint16_t debounceDelay = 5;

            //int buttonState = 0;                        //used to determine which color should be on

            while(1){
                //Button ONE LOOP
                while(button1_Debounce(debounceDelay)){
                    switch(state){
                    case OFF:
                        if(button1_Debounce(debounceDelay))
                              state = GREEN;
                        if(button2_Debounce(debounceDelay))
                              state = RED;
                        break;
                    case GREEN:
                        P3->OUT     |= BIT2;                                        //red ON
                        P3->OUT     &= ~BIT3;                                       //blue OFF
                        P4->OUT     &= ~BIT3;                                       //green OFF
                        SysTick_Delay(timeDelay);                                   //Delay before changing state
                        state   =   YELLOW;
                        if (!(button1_Debounce(debounceDelay)))break;               //breaks out of loop and stay in the state
                    case YELLOW:
                        P3->OUT     |= BIT3;                                        //green ON
                        P4->OUT     &= ~BIT3;                                       //red OFF
                        P3->OUT     &= ~BIT2;                                       //blue OFF
                        SysTick_Delay(timeDelay);
                        state   =   RED;
                        if (!(button1_Debounce(debounceDelay)))break;
                    case RED:
                        P4->OUT     |= BIT3;                                        //blue ON
                        P3->OUT     &= ~BIT3;                                       //green OFF
                        P3->OUT     &= ~BIT2;                                       //red OFF
                        SysTick_Delay(timeDelay);
                        state   =   GREEN;
                        if (!(button1_Debounce(debounceDelay)))break;
                    }
                }
                //BUTTON 2 LOOP
                while(button2_Debounce(debounceDelay)){
                    switch(state){
                         case OFF:
                             if(button1_Debounce(debounceDelay))
                                      state = GREEN;
                             if(button2_Debounce(debounceDelay))
                                      state = RED;
                                            break;
                         case GREEN:
                               P3->OUT     |= BIT2;                                        //green ON
                               P3->OUT     &= ~BIT3;                                       //yellow OFF
                               P4->OUT     &= ~BIT3;                                       //green OFF
                               SysTick_Delay(timeDelay);                                   //Delay before changing state
                               state    =   RED;
                               if (!button2_Debounce(debounceDelay))break;                 //Breaks out of while loop and stay in this state
                        case YELLOW:
                                P3->OUT     |= BIT3;                                        //YELLOw ON
                                P4->OUT     &= ~BIT3;                                       //red OFF
                                P3->OUT     &= ~BIT2;                                       //green OFF
                                SysTick_Delay(timeDelay);
                                state   =   GREEN;
                                if (!button2_Debounce(debounceDelay))break;
                        case RED:
                                P4->OUT     |= BIT3;                                        //red ON
                                P3->OUT     &= ~BIT3;                                       //yellow OFF
                                P3->OUT     &= ~BIT2;                                       //green OFF
                                SysTick_Delay(timeDelay);
                                state   =   YELLOW;
                                if (!button2_Debounce(debounceDelay))break;
                    }
                }
            }
}
//This function initializes a SysTick timer and sets it to run for 1 millisecond
void  SysTick_Init(){
    SysTick->LOAD   =   3000 - 1;               //1 Millisecond
    SysTick->VAL    =   21;                     //Clears with any value
    SysTick->CTRL   =   BIT0;                   //Enables Clock
    while(1){
        if(SysTick->CTRL&BIT(16))               //Delays until Clock ends
            break;
    }
}
//This functions accepts an amount of milliseconds and calls a function to delay
//the given amount of milliseconds desired
void SysTick_Delay(uint16_t delayms){
    int i;
    for(i = 0; i <= delayms; i++){
        SysTick_Init();
    }
}
//this function is used to debounce the button. it will first check if the button is pressed
// it will then delay and check the buttons state again
int button1_Debounce(uint16_t delayTime){
    if(!(P2->IN & BIT7)){                                   //checks if the button is pressed
            SysTick_Delay(delayTime);                       //delay the time
                if(!(P2->IN & BIT7)){                        //Checks if button is pressed again
                        return 1;                           //returns one if button was pressed
                }
    }
    return 0;                                               //returns 0 if button is not pressed
}
//this function is used to debounce the button. it will first check if the button is pressed
// it will then delay and check the buttons state again
int button2_Debounce(uint16_t delayTime){
    if(!(P2->IN & BIT6)){                                   //checks if the button is pressed
            SysTick_Delay(delayTime);                       //delay the time
                if(!(P2->IN & BIT6)){                        //Checks if button is pressed again
                        return 1;                           //returns one if button was pressed
                }
    }
    return 0;                                               //returns 0 if button is not pressed
}

