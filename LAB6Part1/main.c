/**********************************
 * Wade Callahan
 * Prof. Ekin
 * EGR 226 LAB 6
 *This program wil accept user input from a keypad and print to console
 *The program should only print one time for each button press.
 *Debouncing is used to ensure accurate button presses
 *********************************/

//MACROS
#include "msp.h"
#include <stdio.h>
#define ASTERIK 19
#define POUND 21
#define START 657

//Function prototypes
void keypadSetup();
void Read_Keypad(void);
void printKey();
int button_Debounce(uint16_t delay, int bit);
void  SysTick_Init();
void SysTick_Delay(uint16_t delayms);

//global variable for key presses
int keyPressed = START;

void main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;     // stop watchdog timer

    keypadSetup();                                  //sets up keypad

    //main loop
    while(1){
        Read_Keypad();                              //read the keypad
        printKey();                                 //print the key input
    }
}

//this function sets up the keypad
void keypadSetup(){
    P4->SEL0    &= ~(BIT0|BIT1|BIT2|BIT3|BIT4|BIT5|BIT6);
    P4->SEL1    &= ~(BIT0|BIT1|BIT2|BIT3|BIT4|BIT5|BIT6);
    P4->DIR     &= ~(BIT0|BIT1|BIT2|BIT3|BIT4|BIT5|BIT6);
    P4->REN     |=  (BIT0|BIT1|BIT2|BIT3|BIT4|BIT5|BIT6);
    P4->OUT     |=  (BIT0|BIT1|BIT2|BIT3|BIT4|BIT5|BIT6);
}

//This function reads the keypad and store
//key presses into a global variable
void Read_Keypad(void)
{
      int bit = 0b00000001;                                             //used for cycling through P4.0-4.4
      uint16_t debounceDelay = 5;                                       //debounce delay

      //COLUMN 1 KEYS
      P4->OUT   &= ~BIT4;                                               //Set default state to a 0
      P4->DIR   |=  BIT4;                                               //Change to an output
      while(button_Debounce(debounceDelay   ,   bit))                   //Checks if key 1 is press, stays here until button is let go
         keyPressed     =   1;                                          //changes key press
      bit<<=1;                                                          //shifts bit to left 1

      while(button_Debounce(debounceDelay   ,   bit))                   //Checks p4.1
          keyPressed    =   4;
      bit<<=1;

      while(button_Debounce(debounceDelay   ,   bit))                   //checks p4.2
          keyPressed    =   7;
      bit<<=1;

      while(button_Debounce(debounceDelay   ,   bit))                   //checks p4.3
         keyPressed     =   ASTERIK;
      P4->DIR   &= ~BIT4;                                               //Change to an input
      P4->OUT   |=  BIT4;                                               //Set default state to a 1
      bit = 0b00000001;

      //COLUMN 2 KEYS
      P4->OUT   &= ~BIT5;                   //Set default state to a 0
      P4->DIR   |=  BIT5;                   //Change to an output (that is 0 due to previous line)
      while(button_Debounce(debounceDelay   ,   bit))
          keyPressed    =   2;
      bit<<=1;

      while(button_Debounce(debounceDelay   ,   bit))
          keyPressed    =   5;
      bit<<=1;

      while(button_Debounce(debounceDelay   ,   bit))
          keyPressed    =   8;
      bit<<=1;

      while(button_Debounce(debounceDelay   ,   bit))
          keyPressed    =   0;

      P4->DIR   &= ~BIT5;                   //Change to an input
      P4->OUT   |=  BIT5;                   //Set default state to a 1 (pull up)
      bit = 0b00000001;

      //COLUMN 3 KEYS
      P4->OUT   &= ~BIT6;                   //Set default state to a 0
      P4->DIR   |=  BIT6;                   //Change to an output (that is 0 due to previous line)

      while(button_Debounce(debounceDelay   ,   bit))
          keyPressed    =   3;
      bit<<=1;

      while(button_Debounce(debounceDelay   ,   bit))
          keyPressed    =   6;
      bit<<=1;

      while(button_Debounce(debounceDelay   ,   bit))
          keyPressed    =   9;
      bit<<=1;

      while(button_Debounce(debounceDelay   ,   bit))
          keyPressed    =   POUND;
      P4->DIR   &= ~BIT6;                   //Change to an input
      P4->OUT   |=  BIT6;                   //Set default state to a 1 (pull up)
      bit = 0b00000001;
}
//This function prints out the value of keyPressed to the console
//it also prompts the user for input
void printKey(){
    switch(keyPressed){
    case START:
        printf("Please enter a key on the keypad!\n");                      //prompts user for input
        keyPressed = -1;                                                    //sets keypressed to -1
        break;
    case 0:
        printf("Button 0 is pressed!\n");                                   //prints statement when 0 is press
        keyPressed = -1;
        break;
    case 1:
        printf("Button 1 is pressed!\n");
        keyPressed = -1;
        break;
    case 2:
        printf("Button 2 is pressed!\n");                                   //prints statement when 2 is pressed
        keyPressed = -1;
        break;
    case 3:
        printf("Button 3 is pressed!\n");
        keyPressed = -1;
        break;
    case 4:
        printf("Button 4 is pressed!\n");                                   //prints statement when 4 is pressed
        keyPressed = -1;
        break;
    case 5:
        printf("Button 5 is pressed!\n");
        keyPressed = -1;
        break;
    case 6:
        printf("Button 6 is pressed!\n");                                   //prints statement when 6 is pressed
        keyPressed = -1;
        break;
    case 7:
        printf("Button 7 is pressed!\n");
        keyPressed = -1;
        break;
    case 8:
        printf("Button 8 is pressed!\n");                                   //prints statement when 8 is pressed
        keyPressed = -1;
        break;
    case 9:
        printf("Button 9 is pressed!\n");
        keyPressed = -1;
        break;
    case ASTERIK:
        printf("Button * is pressed!\n");                                   //prints statement when * is pressed
        keyPressed = -1;
        break;
    case POUND:
        printf("Button # is pressed!\n");
        keyPressed = -1;
        break;
    }
}

//This function initializes a SysTick timer and sets it to run for 1 millisecond
void  SysTick_Init(){
    SysTick->LOAD   =   3000 - 1;               //1 Millisecond
    SysTick->VAL    =   21;                     //Clears with any value
    SysTick->CTRL   =   BIT0;                   //Enables Clock
    while(1){
        if(SysTick->CTRL&BIT(16))               //Delays until Clock ends
            break;
    }
}
//This functions accepts an amount of milliseconds and calls a function to delay
//the given amount of milliseconds desired
void SysTick_Delay(uint16_t delayms){
    int i;
    for(i = 0; i <= delayms; i++){
        SysTick_Init();
    }
}
//this function is used to debounce the button. it will first check if the button is pressed
// it will then delay and check the buttons state again
int button_Debounce(uint16_t delayTime, int bit){
    if(!(P4->IN & bit)){                                   //checks if the button is pressed
            SysTick_Delay(delayTime);                       //delay the time
                if(!(P4->IN & bit)){                        //Checks if button is pressed again
                        return 1;                           //returns one if button was pressed
                }
    }
    return 0;                                               //returns 0 if button is not pressed
}
