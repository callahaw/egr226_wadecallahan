#include "msp.h"
#include <stdint.h>



/*The following two arrays are similar to what StackOverflow user Christian Glass
 * used for the MSP430. The display method was modified and is different from the stackoverflow code
 *  Also, no interupts were used in the stackoverflow code.
 * URL: https://stackoverflow.com/questions/56510127/creating-a-7-segment-led-displaying-two-numbers-from-0-to-99-using-msp430-laun
 */
int segments[8] = {BIT0, BIT1, BIT2, BIT3, BIT4, BIT5, BIT6, BIT7};

int sevenSegArray[10][7] = {
                                    { 1,1,1,1,1,1,0 },                              // digit 0
                                    { 0,1,1,0,0,0,0 },                              // digit 1
                                    { 1,1,0,1,1,0,1 },                              // digit 2
                                    { 1,1,1,1,0,0,1 },                              // digit 3
                                    { 0,1,1,0,0,1,1 },                              // digit 4
                                    { 1,0,1,1,0,1,1 },                              // digit 5
                                    { 1,0,1,1,1,1,1 },                              // digit 6
                                    { 1,1,1,0,0,0,0 },                              // digit 7
                                    { 1,1,1,1,1,1,1 },                              // digit 8
                                    { 1,1,1,0,0,1,1 }                               // digit 9
                                    };

unsigned int input = 0;

void sevenSegDisplay(int input);
void delayMS(int delay);
void  SysTick_Init();

int main(void) {
    __disable_irq();

    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;

    SysTick_Init();

    P4->SEL1 &= ~0xFF;
    P4->SEL0 &= ~0xFF;
    P4->DIR |= 0xFF;                                // set pins as outputs
    //button 1
    P2->SEL0    &=  ~BIT7;                          //gpio P2.7
    P2->SEL1    &=  ~BIT7;                          //GPIO P2.7
    P2->DIR     &=  ~BIT7;                          //direction is input (0)
    P2->REN     |=  BIT7;                           //Enable resistor
    P2->OUT     |=  BIT7;                           //set default state to a 1
    //button 2
    P2->SEL0    &=  ~BIT6;                          //gpio P2.6
    P2->SEL1    &=  ~BIT6;
    P2->DIR     &=  ~BIT6;                          //direction is input (0)
    P2->REN     |=  BIT6;                           //enable resistor
    P2->OUT     |=  BIT6;                           //set default state to a 1

    //Interupt setup
    P2->IES     |=  BIT6|BIT7;
    P2->IE      |=  BIT6|BIT7;
    P2->IFG = 0;                                //clears IFG

    NVIC_EnableIRQ(PORT2_IRQn);                 //enable interupt
    __enable_irq();

    sevenSegDisplay(input);

    while(1){
    }
}


//Port 2 IR handler
//Changes display of seven segment based on user input
void PORT2_IRQHandler(void){
    if(P2->IFG & BIT6 ){                                            //checks if 2.6
        delayMS(150);                                               //kind of debounces
        sevenSegDisplay(input);                                     //display appropriate segments
        input++;                                                    //increment the input
        if(input == 10)                                             //reset input after 9
           input = 0;                                               //reset input

    }
    if(P2->IFG & BIT7 ){                                            //2.7
        delayMS(150);                                               //debounce
        input--;                                                    //decrement the input
        sevenSegDisplay(input);                                     //Display
        if(input == 0)                                              //reset input at 0
           input = 10;                                              //reset input
    }
    P2->IFG &= ~BIT6;                                               //clears 2.6
    P2->IFG &= ~BIT7;                                               //clears 2.7
    P2->IFG = 0;                                                    //clears
}
//Initializes the systick timer
void  SysTick_Init(){
    SysTick->LOAD   =   3000 - 1;                   //1 Millisecond
    SysTick->VAL    =   21;                         //Clears with any value
    SysTick->CTRL   =   BIT0;                       //Enables Clock
    while((SysTick->CTRL & BIT(16))==0);            //wait for delay time to elapse
}
//This functions accepts an amount of milliseconds and calls a function to delay
//the given amount of milliseconds desired
void delayMS(int delay){
    SysTick->LOAD = (delay * 3000 - 1);             //3000 clocks = 1 milii
    SysTick->VAL = 0;                               //puts time to zero
    while((SysTick->CTRL & BIT(16))==0);            //wait for delay time to elapse
}
//This function sifts through the array and determines which segments should be driven high
//Accepts an input as a parameter
void sevenSegDisplay(int input){
    int i;                                                  //Loop variable
    for (i = 0; i < 7; i++){                                //starts at zero and increments to 7 (number of segment options)
        if(sevenSegArray[input][i] == 1)                    //Checks if the array has a 1 at location
            P4OUT |= segments[i];                           //Drives segment high if it is a one
        else
            P4OUT &= ~segments[i];                          //drives segment low if it is a zero
    }
}


