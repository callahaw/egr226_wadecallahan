/*Wade Callahan
 * Prof Ekin
 * EGR 226 Lab 10
 * This program will display temperture reading to an LCD.
 */


/** PINS USED:
 * DB4-7    ->  P4.4-P4.7
 * E        ->  P4.2
 * RW       ->  P4.1
 * RS       ->  P4.3
 */

//Libraries
#include "msp.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

//Function prototypes
void  SysTick_Init();
void delayMS(uint16_t delay);
void delayUS(uint8_t delay);
void cmdWrite(uint8_t command);
void pulseEN();
void pushNibble(uint8_t nibble);
void pushByte(uint8_t byte);
void dataWrite(uint8_t data);
void LCD_Init();
void writeString(char string[]);
void printLCD(void);
void ADC14int (void);




void main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;     // stop watchdog timer


    SysTick_Init();                                 //initialize the systick timer

    LCD_Init();                                     //Part 1, initilizes the lcd, blinks cursor


    printLCD();

    //Nothing goes gets looped :(
    while(1){
                                        //Part 2, prints info to screen
    }
}

//This function initializes a SysTick timer and sets it to run for 1 millisecond
void  SysTick_Init(){
    SysTick->LOAD   =   3000 - 1;                   //1 Millisecond
    SysTick->VAL    =   21;                         //Clears with any value
    SysTick->CTRL   =   BIT0;                       //Enables Clock
    while((SysTick->CTRL & BIT(16))==0);            //wait for delay time to elapse
}
//This functions accepts an amount of milliseconds and calls a function to delay
//the given amount of milliseconds desired
void delayMS(uint16_t delay){
    SysTick->LOAD = (delay * 3000 - 1);            //3000 clocks = 1 milii
    SysTick->VAL = 0;                           //puts time to zero
    while((SysTick->CTRL & BIT(16))==0);       //wait for delay time to elapse
}
//This functions accepts an amount of microseconds and calls a function to delay
//the given amount of microseconds desired
void delayUS(uint8_t delay){
    SysTick->LOAD = (delay * 3 - 1);            //3 = clocks = 1 micro
    SysTick->VAL = 0;                           //puts time to zero
    while((SysTick->CTRL & BIT(16))==0);       //wait for delay time to elapse
}
//Function is used to write a command to the LCD, RS is set to zero
void cmdWrite(uint8_t command){
    P4->OUT     &= ~BIT3;                       //send command, RS = 0
    delayMS(1);
    pushByte(command);                          //calls pushByte
}
//Function is used to write data to the LCD, RS is set to one
void dataWrite(uint8_t data){
    P4->OUT     |= BIT3;                        //send Data, RS = 1
    delayMS(1);
    pushByte(data);
}
//Calls push nibble, used to seperate the two nibbles in a byte so that
//they can be sent to the scenn.
void pushByte(uint8_t byte){
    int top, bottom;
    top = (byte & 0xF0) >> 4;                       //sets to the top four bits
    pushNibble(top);                                //call pushNibble
    delayMS(1);                                     //delays 1 milli
    bottom = byte & 0x0F;                           //sets to the bottom four bits
    pushNibble(bottom);
    delayMS(1);
}
//Push a nibble to screen, clears top four bit in output,
//Pushes desired nibble to the screen, pulse enable
void pushNibble(uint8_t nibble){
    P4->OUT &= ~0xF0;
    P4->OUT |= (nibble & 0x0F) << 4;
    pulseEN();
}

//Pulses the enable pin (P4,2), first sets low,
//delays, drives high, delay, and drive low again
//used to send command or data
void pulseEN(){
    P4->OUT     &= ~BIT2; //Enable Low
    delayUS(10);
    P4->OUT     |= BIT2; //Enable High
    delayUS(10);
    P4->OUT     &= ~BIT2; //Enable Low
    delayUS(10);
}
//Initializes the LCD, DONT CHANGE THE DELAYS
//PINS SETUP in here
void LCD_Init(){
/** PINS USED:
 * DB4-7 -> P4.4-P4.7
 * E -> P4.2
 * RW -> P4.1
 * RS -> P4.3
 */

    P4->SEL0 &= ~(0xFE); //(0b11111110)
    P4->SEL1 &= ~(0xFE); //GPIO
    P4->DIR  |=  (0xFE); //Output
    P4->OUT  &= ~(0xFE); //Initially zero (including RW)

    delayMS(60);

    cmdWrite(0x03);                                 // reset sequence, do three times, dont change delays!
    delayMS(10);
    cmdWrite(0x03);
    delayUS(200);
    cmdWrite(0x03);
    delayMS(10);

    cmdWrite(0x02);                                 // setting 4 bit mode
    delayUS(100);

    cmdWrite(0x08);                                 // 5x7 Format
    delayUS(100);

    cmdWrite(0x0F);                                 // 0x0F = blinking cursor or 0x0C = no crusor
    delayUS(100);

    cmdWrite(0x01);                                 // clear display, move cursor home
    delayUS(100);

    cmdWrite(0x06);                                 // increment cursor
    delayUS(100);
}

//Function accepts a scrint and writes the data to the LCD one character at a time
void writeString(char string[]){
    int i;
    for(i=0;    i<strlen(string);   i++)        //loop for the length of the string
    {
        dataWrite(string[i]);
    }
}

void printLCD(void){
    //Strings to be printed
    cmdWrite(1);                                //clear display
    delayMS(1);
    static volatile uint16_t result;
    float nADC;
    float temp;
    char tempString[6];
    char str1[16] = "Current temp. is";

    cmdWrite(0x00);                             //put to line address at line 1 spot 07 (0x80 + 0x06)
    writeString(str1);
    ADC14int ();
    cmdWrite(1);                                //clear display
    delayMS(1);

    while(1){
        cmdWrite(1);                                                    //put to line address at line 1 spot 07 (0x80 + 0x06)
        ADC14->CTL0 |= 1;                                               //start conversation
        while(!ADC14->IFGR0);                                           //wait for conversation to complete
        result = ADC14->MEM[0];                                         // get the value from the ADC
        nADC = result * .2014;
        temp = ((nADC) - 500)/100;
        sprintf(tempString, "%f%d\n", temp, 0b11011111);                //puts temp to string with degree symbol = 0b1101 1111
        printf("%s\n", tempString);                                     //put to line 2
        writeString(tempString);
        delayMS(5000);
    }
}
//Initilizes the Analog to digital converter
void ADC14int (void)
{
        P5SEL0 |= 0X20;                     // configure pin 5.5 for A0 input
        P5SEL1 |= 0X20;
        ADC14->CTL0 &=~ 0x00000002;          // disable ADC14ENC during configuration
        ADC14->CTL0   |=  0x04200210;           // S/H pulse mode, SMCLK, 16 sample clocks
        ADC14->CTL1    =   0x00000030;          // 14 bit resolution
        ADC14->CTL1   |=   0x00000000;          // Selecting ADC14CSTARTADDx mem0 REGISTER
        ADC14->MCTL[0] = 0x00000000;            // ADC14INCHx = 0 for mem[0]
        // ADC14->MCTL[0] = ADC14_MCTLN_INCH_0;
        ADC14->CTL0 |= 0x00000002;          // enable ADC14ENC, starts the ADC after configuration
}
