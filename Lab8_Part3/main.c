#include "msp.h"


void keypadSetup();
void Read_Keypad(void);
//void storeKey();
//void printKeys(int pinCode[]);
int button_Debounce(uint16_t delay, int bit);
void  SysTick_Init();
void SysTick_Delay(uint16_t delayms);


void pwminit(uint16_t period, uint16_t periodON);

void main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;

    uint16_t dutyCycle = 50;
    uint16_t period = 60000;
    keypadSetup();



    pwminit(period, (period * dutyCycle)/100);

    while (1) {
        Read_Keypad();
    }
}

void pwminit(uint16_t period, uint16_t periodON){


    P2->SEL0            |=  BIT4;                                                       //GPIO for PWM
    P2->SEL1            &=  ~BIT4;
    P2->DIR             =   BIT4;                                                       //OUTPUt

    TIMER_A0->CTL       =   TIMER_A_CTL_SSEL__SMCLK |                                   // SMCLK
                            TIMER_A_CTL_ID_3        |                                   // divide by 8
                            TIMER_A_CTL_MC_1;                                           // Up mode

    //LOADS Timer
    TIMER_A0->CCR[0]    = period;
    TIMER_A0->CCTL[1]   = 0b11100000;                                                   //reset/set
    TIMER_A0->CCR[1]    = periodON;
}

void keypadSetup(){
    P4->SEL0    &= ~(BIT0|BIT1|BIT2|BIT3|BIT4|BIT5|BIT6);
    P4->SEL1    &= ~(BIT0|BIT1|BIT2|BIT3|BIT4|BIT5|BIT6);
    P4->DIR     &= ~(BIT0|BIT1|BIT2|BIT3|BIT4|BIT5|BIT6);
    P4->REN     |=  (BIT0|BIT1|BIT2|BIT3|BIT4|BIT5|BIT6);
    P4->OUT     |=  (BIT0|BIT1|BIT2|BIT3|BIT4|BIT5|BIT6);
}

//This function will read in values from the keypad and store
//them in a global variable
void Read_Keypad(void)
{
      keypadSetup();
      int bit = 0b00000001;
      uint16_t debounceDelay = 5;
      uint16_t period = 60000;

      //COLUMN 1 KEYS
      P4->OUT   &= ~BIT4;                                               //Set default state to a 0
      P4->DIR   |=  BIT4;                                               //Change to an output
      while(button_Debounce(debounceDelay   ,   bit))                  //Checks if key 1 is press
          pwminit(period, (period * 10)/100);                           //sets up new PWM
      bit<<=1;                                                          //shifts bit over 1 spot

      while(button_Debounce(debounceDelay   ,   bit))                   //check if P4.1 is active
          pwminit(period, (period * 40)/100);
      bit<<=1;

      while(button_Debounce(debounceDelay   ,   bit))                   //checks if p4.2 is active
          pwminit(period, (period * 70)/100);
      bit<<=1;

      while(button_Debounce(debounceDelay   ,   bit))                   //checks if P4.3 is active
         //keyPressed     =   ASTERIK;
      P4->DIR   &= ~BIT4;                                               //Change to an input
      P4->OUT   |=  BIT4;                                               //Set default state to a 1
      bit = 0b00000001;                                                 //resets bit

      //COLUMN 2 KEYS
      P4->OUT   &= ~BIT5;                   //Set default state to a 0
      P4->DIR   |=  BIT5;                   //Change to an output (that is 0 due to previous line)
      while(button_Debounce(debounceDelay   ,   bit))
          pwminit(period, (period * 20)/100);
      bit<<=1;

      while(button_Debounce(debounceDelay   ,   bit))
          pwminit(period, (period * 50)/100);
      bit<<=1;

      while(button_Debounce(debounceDelay   ,   bit))
          pwminit(period, (period * 80)/100);
      bit<<=1;

      while(button_Debounce(debounceDelay   ,   bit))
          pwminit(period, (period * 0)/100);

      P4->DIR   &= ~BIT5;                   //Change to an input
      P4->OUT   |=  BIT5;                   //Set default state to a 1 (pull up)
      bit = 0b00000001;

      //COLUMN 3 KEYS
      P4->OUT   &= ~BIT6;                   //Set default state to a 0
      P4->DIR   |=  BIT6;                   //Change to an output (that is 0 due to previous line)

      while(button_Debounce(debounceDelay   ,   bit))
          pwminit(period, (period * 30)/100);
      bit<<=1;

      while(button_Debounce(debounceDelay   ,   bit))
          pwminit(period, (period * 60)/100);
      bit<<=1;

      while(button_Debounce(debounceDelay   ,   bit))
          pwminit(period, (period * 90)/100);
      bit<<=1;

      while(button_Debounce(debounceDelay   ,   bit))
          //keyPressed    =   POUND;
      P4->DIR   &= ~BIT6;                   //Change to an input
      P4->OUT   |=  BIT6;                   //Set default state to a 1 (pull up)
      bit = 0b00000001;
}

void  SysTick_Init(){
    SysTick->LOAD   =   3000 - 1;               //1 Millisecond
    SysTick->VAL    =   21;                     //Clears with any value
    SysTick->CTRL   =   BIT0;                   //Enables Clock
    while(1){
        if(SysTick->CTRL&BIT(16))               //Delays until Clock ends
            break;
    }
}
//This functions accepts an amount of milliseconds and calls a function to delay
//the given amount of milliseconds desired
void SysTick_Delay(uint16_t delayms){
    int i;
    for(i = 0; i <= delayms; i++){
        SysTick_Init();
    }
}
//this function is used to debounce the button. it will first check if the button is pressed
// it will then delay and check the buttons state again
int button_Debounce(uint16_t delayTime, int bit){
    if(!(P4->IN & bit)){                                   //checks if the button is pressed
            SysTick_Delay(delayTime);                       //delay the time
                if(!(P4->IN & bit)){                        //Checks if button is pressed again
                        return 1;                           //returns one if button was pressed
                }
    }
    return 0;                                               //returns 0 if button is not pressed
}

