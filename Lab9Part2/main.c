/**
 *Wade Callahan
 *Prof Ekin
 *EGR 226 Lab 9
 *This program will change the display on a seven segment display.
 *Two external button will be used to change this display. One button
 *will increment the display by one digit and the other will decrement the
 *display by one digit.
 */

#include "msp.h"
#include <stdint.h>

/*The following two arrays are similar to what StackOverflow user Christian Glass
 * used for the MSP430. The display method was modified and is different from the stackoverflow code
 *  Also, no interupts were used in the stackoverflow code.
 * URL: https://stackoverflow.com/questions/56510127/creating-a-7-segment-led-displaying-two-numbers-from-0-to-99-using-msp430-laun
 */

//these segments represent which bit corresponds to the desired segments
//for example, BIT0 -> P4.0, it is the 0th element so it pairs with segment A on the seven segment
int segments[8] = {BIT0, BIT1, BIT2, BIT3, BIT4, BIT5, BIT6, BIT7};

//2-Dimensional array that stores all combinations of segments for each digit
//The rows represent the digit to display and the columns represent
//the segments for each digit.
int sevenSegArray[10][7] = {
                                    { 1,1,1,1,1,1,0 },                              // digit 0
                                    { 0,1,1,0,0,0,0 },                              // digit 1
                                    { 1,1,0,1,1,0,1 },                              // digit 2
                                    { 1,1,1,1,0,0,1 },                              // digit 3
                                    { 0,1,1,0,0,1,1 },                              // digit 4
                                    { 1,0,1,1,0,1,1 },                              // digit 5
                                    { 1,0,1,1,1,1,1 },                              // digit 6
                                    { 1,1,1,0,0,0,0 },                              // digit 7
                                    { 1,1,1,1,1,1,1 },                              // digit 8
                                    { 1,1,1,0,0,1,1 }                               // digit 9
                                    };

//global variable used to determine which digit to display
unsigned int input = 0;

//function prototype
void sevenSegDisplay(int input);

int main(void) {
    __disable_irq();

    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;

    P4->SEL1    &= ~0xFF;
    P4->SEL0    &= ~0xFF;
    P4->DIR     |= 0xFF;                                // set pins as outputs

    SysTick->LOAD   =   3000000-1;                      //1 second in 3MHz symbol
    SysTick->CTRL   =   7;                              //clear

    NVIC_SetPriority(SysTick_IRQn,2);                   //setup ir handler
    __enable_irq();

    while(1){
    }
}

//SysTick Handler, executes every second
void SysTick_Handler(void){
    sevenSegDisplay(input);                 //display to seven segment
    input++;                                //increments the digit
    if(input == 10){                        //resets at 10
        input = 0;                          //reset
    }
}
//This function sifts through the array and determines which segments should be driven high
//Accepts an input as a parameter
void sevenSegDisplay(int input){
    int i;                                                  //Loop variable
    for (i = 0; i < 7; i++){                                //starts at zero and increments to 7 (number of segment options)
        if(sevenSegArray[input][i] == 1)                    //Checks if the array has a 1 at location
            P4OUT |= segments[i];                           //Drives segment high if it is a one
        else
            P4OUT &= ~segments[i];                          //drives segment low if it is a zero
    }
}




