/*Wade Callahan
 * Prof Ekin
 * EGR 226 Lab 7
 * This program will print a string to the screen. it will then move each character
 * in the sstring to the left one spot every second. After the string is completly off
 * screen, it will start to move the string to the right until the string is back on.
 * it will continue this process forever.
 *
 */


/** PINS USED:
 * DB4-7    ->  P4.4-P4.7
 * E        ->  P4.2
 * RW       ->  P4.1
 * RS       ->  P4.3
 */

//Libraries
#include "msp.h"
#include <string.h>

//Function prototypes
void delay_us(int us);
void  SysTick_Init();
void delayMS(uint8_t delay);
void delayUS(uint8_t delay);
void cmdWrite(uint8_t command);
void pulseEN();
void pushNibble(uint8_t nibble);
void pushByte(uint8_t byte);
void dataWrite(uint8_t data);
void LCD_Init();
void writeString(char string[]);


void main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;     // stop watchdog timer

    char printString[] = "LABORATORY OVER ";
    char storeString[] = " REVO YROTAROBAL";

    //init
    SysTick_Init();
    LCD_Init();

    int j, i;

    while(1){
        //Moves each character to the left every second
        for(i=0;i<16;i++) {
            cmdWrite(0x80);                             //address of spot 1 of line 1
            writeString(printString);                   //write the new string

            for(j=0;    j<16;   j++){
                printString[j] = printString[j + 1];   //moves each character to the left one spot
            }
            //Delay 1 second, couldn't do delayMS(1000) :(
            delayMS(250);
            delayMS(250);
            delayMS(250);
            delayMS(250);
        }
       //Reverses order of the string being sent to the LCD
       for(j=0 ;j<16;j++) {
           cmdWrite(0x80);
           writeString(printString);

           for(i=0; i<(j + 1);  i++)
               printString[i] = storeString[j - i];             //moves each character to the right one spot

               //Delay 1 second
               delayMS(250);
               delayMS(250);
               delayMS(250);
               delayMS(250);
      }
    }
}

//This function initializes a SysTick timer and sets it to run for 1 millisecond
void  SysTick_Init(){
    SysTick->LOAD   =   3000 - 1;                   //1 Millisecond
    SysTick->VAL    =   21;                         //Clears with any value
    SysTick->CTRL   =   BIT0;                       //Enables Clock
    while((SysTick->CTRL & BIT(16))==0);            //wait for delay time to elapse
}
//This functions accepts an amount of milliseconds and calls a function to delay
//the given amount of milliseconds desired
void delayMS(uint8_t delay){
    SysTick->LOAD = (delay * 3000 - 1);            //3000 clocks = 1 milii
    SysTick->VAL = 0;                           //puts time to zero
    while((SysTick->CTRL & BIT(16))==0);       //wait for delay time to elapse
}
//This functions accepts an amount of microseconds and calls a function to delay
//the given amount of microseconds desired
void delayUS(uint8_t delay){
    SysTick->LOAD = (delay * 3 - 1);            //3 = clocks = 1 micro
    SysTick->VAL = 0;                           //puts time to zero
    while((SysTick->CTRL & BIT(16))==0);       //wait for delay time to elapse
}
//Function is used to write a command to the LCD, RS is set to zero
void cmdWrite(uint8_t command){
    P4->OUT     &= ~BIT3;                       //send command, RS = 0
    delayMS(1);
    pushByte(command);                          //calls pushByte
}
//Function is used to write data to the LCD, RS is set to one
void dataWrite(uint8_t data){
    P4->OUT     |= BIT3;                        //send Data, RS = 1
    delayMS(1);
    pushByte(data);
}
//Calls push nibble, used to seperate the two nibbles in a byte so that
//they can be sent to the scenn.
void pushByte(uint8_t byte){
    int top, bottom;
    top = (byte & 0xF0) >> 4;                       //sets to the top four bits
    pushNibble(top);                                //call pushNibble
    delayMS(1);                                     //delays 1 milli
    bottom = byte & 0x0F;                           //sets to the bottom four bits
    pushNibble(bottom);
    delayMS(1);
}
//Push a nibble to screen, clears top four bit in output,
//Pushes desired nibble to the screen, pulse enable
void pushNibble(uint8_t nibble){
    P4->OUT &= ~0xF0;
    P4->OUT |= (nibble & 0x0F) << 4;
    pulseEN();
}

//Pulses the enable pin (P4,2), first sets low,
//delays, drives high, delay, and drive low again
//used to send command or data
void pulseEN(){
    P4->OUT     &= ~BIT2; //Enable Low
    delayUS(10);
    P4->OUT     |= BIT2; //Enable High
    delayUS(10);
    P4->OUT     &= ~BIT2; //Enable Low
    delayUS(10);
}
//Initializes the LCD, DONT CHANGE THE DELAYS
//PINS SETUP in here
void LCD_Init(){
/** PINS USED:
 * DB4-7 -> P4.4-P4.7
 * E -> P4.2
 * RW -> P4.1
 * RS -> P4.3
 */

    P4->SEL0 &= ~(0xFE); //(0b11111110)
    P4->SEL1 &= ~(0xFE); //GPIO
    P4->DIR  |=  (0xFE); //Output
    P4->OUT  &= ~(0xFE); //Initially zero (including RW)

    delayMS(60);

    cmdWrite(0x03);                                 // reset sequence, do three times, dont change delays!
    delayMS(10);
    cmdWrite(0x03);
    delayUS(200);
    cmdWrite(0x03);
    delayMS(10);

    cmdWrite(0x02);                                 // setting 4 bit mode
    delayUS(100);

    cmdWrite(0x08);                                 // 5x7 Format
    delayUS(100);

    cmdWrite(0x0F);                                 // 0x0F = blinking cursor or 0x0C = no crusor
    delayUS(100);

    cmdWrite(0x01);                                 // clear display, move cursor home
    delayUS(100);

    cmdWrite(0x06);                                 // increment cursor
    delayUS(100);
}

//Function accepts a scrint and writes the data to the LCD one character at a time
void writeString(char string[]){
    int i;
    for(i=0;    i<strlen(string);   i++)        //loop for the length of the string
    {
        dataWrite(string[i]);
    }
}
