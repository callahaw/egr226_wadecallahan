/**
 *Wade Callahan
 *Prof Ekin
 *EGR 226 Lab 8
 *This function will set a pwm based on input from debugging console
 *The functio uses P2.4 which is associated with TA0.1
 */

#include "msp.h"

//functionn prototype
void pwminit(uint16_t period, uint16_t periodON);

void main(void)
{
	WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;		// stop watchdog timer

	uint16_t dutyCycle = 10;                        //Duty Cycle
	uint16_t period = 60000;                        //Period, 60000 cycle = 20 ms = 50Hz frequency


	pwminit(period, dutyCycle);                     //function call

	    while (1);

}

//This function is used to generate a PWM using TimerA
//The function accepts a period and a duty cycle as its input
//The function setsup the pins associatied with TA0.1
void pwminit(uint16_t period, uint16_t dutyCycle){


    P2->SEL0            |=  BIT4;                                                       //GPIO for PWM
    P2->SEL1            &=  ~BIT4;
    P2->DIR             =   BIT4;                                                       //OUTPUt

    TIMER_A0->CTL       =   TIMER_A_CTL_SSEL__SMCLK |                                   // SMCLK
                            TIMER_A_CTL_ID_3        |                                   // divide by 8
                            TIMER_A_CTL_MC_1;                                           // Up mode

    TIMER_A0->CCR[0]    = period;                                                       //Loads the period
    TIMER_A0->CCTL[1]   = 0b11100000;                                                   //reet/set

    while(1)
        TIMER_A0->CCR[1]    = (period * dutyCycle / 100);                               //loads the On time continously for debugging purposes

}

