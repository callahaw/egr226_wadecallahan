/*
 *Wade Callahan
 *Prof. Ekin
 *EGR 226 LAB
 *LAB_1_StatsLib
 *Description: This lab is intended to be a C programming refresher. A library will be created
 *and utilized to calculate statistics of a data set. INPUT will be taken in from a txt file. The
 *program will then output to the console the max, min, mean, median, variance, and standard deviation
 *of the data in the text file.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#include <string.h>

//function is used to find the largest number in a data set
float maximum(float nums[], int n){

    //initialize max to the first value in array
    float max = nums[0];

    int i;

    for (i = 1; i < n; i++)
        if (nums[i] > max) max = nums[i];

    return max;
}

//function is used to find the smallest number in a data set
float minimum(float nums[], int n){

    //initialize min to the first value in array
    float min = nums[0];

    int i;

    for (i = 1; i < n; i++)
        if (nums[i] < min) min = nums[i];

    return min;
}

//function is used to calculate the mean in a data set
float mean(float nums[], int n){
    int i;

    float total = 0, mean = 0;
    //calculates total sum
    for( i = 0; i < n; i++)
        total += nums[i];

    //divide total sum by number of entries
    mean = total / n;

    return mean;
}

//function is used to calculate the median in a data set
float median(float nums[], int n){
    int i, j, temp = 0;

    //Sorting algorithm to sort numbers in ascending order
    for (i = 0; i < n; i++){

        for(j = i + 1; j < n; j++){

            if(nums[i] > nums[j]){

                temp = nums[i];
                nums[i] = nums[j];
                nums[j] = temp;
            }
        }
    }

    //avgs two middle numbers if even num of entries
    if(n%2 == 0)
        return (nums[(n-1)/2] + nums[n/2])/2;
    else
        return nums[n/2];
}

//function is used to calculate the variance in a data set
float variance(float nums[], int n){
    float avg;
    float variSum = 0;
    int i;

    avg = mean(nums, n);

    for(i = 0; i < n; i++)
        variSum += ((nums[i] - avg) * (nums[i] - avg));

    return variSum / (n - 1);
}

//function is used to calculate the standard deviation in a data set
float standard_deviation(float nums[], int n){
    return sqrt(variance(nums, n) );
}

