/*
 *Wade Callahan
 *Prof. Ekin
 *EGR 226 LAB
 *LAB_1_StatsLib
 *Description: This lab is intended to be a C programming refresher. A library will be created
 *and utilized to calculate statistics of a data set. INPUT will be taken in from a txt file. The
 *program will then output to the console the max, min, mean, median, variance, and standard deviation
 *of the data in the text file.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#include <string.h>
#include "stats_lib.h"

int main(){

    FILE *fp;
    int entryAmt = 0, i;
    float temp; //used to scan in while counting amt in data set

    fp = fopen("data.txt", "r");

    //checks for valid file
    if(!fp){
        printf("Error: File not found!");
        exit(1);
    }else{
        //counts the amt of entries in file
        while(fscanf(fp, "%f", &temp)!= EOF)
            entryAmt++;
    }

    //closes and reopens the file
    fclose(fp);
    fp = fopen("data.txt", "r");

    //array to store all data in file
    float data[entryAmt];

    //scans in data
    for(i = 0; i < entryAmt; i++)
        fscanf(fp, "%f", &data[i]);
    fclose(fp);

    //prints out the desired output
    printf("Maximum: %g\n", maximum(data, entryAmt));
    printf("Minimum: %g\n", minimum(data, entryAmt));
    printf("Mean: %g\n", mean(data, entryAmt));
    printf("Median: %g\n", median(data, entryAmt));
    printf("Variance: %g\n", variance(data, entryAmt));
    printf("Standard Deviation: %g\n", standard_deviation(data, entryAmt));

    return 0;
}
