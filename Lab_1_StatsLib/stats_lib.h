/*
 *Wade Callahan
 *Prof. Ekin
 *EGR 226 LAB
 *LAB_1_StatsLib
 *Description: This lab is intended to be a C programming refresher. A library will be created
 *and utilized to calculate statistics of a data set. INPUT will be taken in from a txt file. The
 *program will then output to the console the max, min, mean, median, variance, and standard deviation
 *of the data in the text file.
 */


#ifndef STATS_LIB_H_
#define STATS_LIB_H_
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#include <string.h>

float maximum(float nums[ ], int n);
float minimum (float nums[ ], int n);
float mean(float nums[ ], int n);
float median(float nums[ ], int n);
float variance(float nums[ ], int n);
float standard_deviation(float nums[ ], int n);

#endif /* STATS_LIB_H_ */
